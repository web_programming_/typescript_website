let r: unknown = 'hello';
let num: string = "1";
console.log((r as string).length);
console.log((<string>r).length);
console.log(((num as unknown)as number).toFixed(2));
